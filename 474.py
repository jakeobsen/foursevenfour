#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright 2019 Morten Jakobsen. All rights reserved.
# Use of this source code is governed by a BSD-style
# license that can be found in the LICENSE file.

from datetime import datetime
import sqlite3
import argparse
from os import path
from sys import exit


class fourSevenFour:
    """
    Four Seven Four is a time tracking class, that can keep track of time
    in a sqlite database.

    tt = fourSevenFour(args.file)
    """

    def __init__(self, dbName):
        """
        Initializes class and sets database name

        :param dbName: database filename
        :return: None
        """
        self.dbName = dbName

    def setDailyMinutes(self, dailyMinutes):
        """
        Sets the daily amount of work minutes

        :return: None
        """
        self.dailyWorkMinutes = dailyMinutes

    def connect(self):
        """
        Connects to sqlite database

        :return: None
        """
        self.sqlite = sqlite3.connect(self.dbName)

    def validateDatabase(self):
        """
        """
        result = True
        self.connect()
        if path.exists(self.dbName):
            try:
                self.sqlite.execute("SELECT name FROM sqlite_master WHERE type='table';")
            except Exception as e:
                print(e)
                result = False
        return result

    def close(self):
        """
        Closes connection to sqlite database

        :return: None
        """
        self.sqlite.close()

    def cursor(self):
        """
        Gets sqlite cursor from database connection 

        :return: None
        """
        return self.sqlite.cursor()

    def commit(self):
        """
        Commits changes to sqlite database

        :return: None
        """
        return self.sqlite.commit()

    def createDatabase(self):
        """ 
        Creates database if it does not exists

        :return: None
        """
        c = self.cursor()
        c.execute('''
        CREATE TABLE IF NOT EXISTS "timelog" (
            "id" INTEGER PRIMARY KEY AUTOINCREMENT,
            "t1" TEXT,
            "t2" TEXT,
            "comment" TEXT)
        ''')
        self.commit()

    def getLastEntry(self):
        """
        Gets last timelog entry in database

        :return: Returns row from database
        """
        c = self.cursor()
        c.execute('SELECT * FROM timelog ORDER BY id DESC LIMIT 1')
        return c.fetchone()

    def clockIn(self):
        """
        Clock in, if not already clocked in

        :return: None
        """
        c = self.cursor()
        lastLine = self.getLastEntry()
        if lastLine is None or lastLine[2] is not None:
            c.execute('INSERT INTO timelog (t1) VALUES (?)',
                      (str(datetime.now()),))
            self.commit()
            print("You are now clocked in!")
        else:
            print("Already clocked in!")

    def clockOut(self, comment):
        """
        Clock out, if clocked in

        :return: None
        """
        c = self.cursor()
        lastLine = self.getLastEntry()
        if lastLine is None or lastLine[2] is None:
            c.execute('UPDATE timelog SET t2=?, comment=? WHERE t2 is NULL',
                      (str(datetime.now()), comment,))
            self.commit()
            print("You are now clocked out!")
        else:
            print("You are not clocked in!")

    def reportObject(self, date):
        """
        Print a report documenting hours tracked

        :return: None
        """
        c = self.cursor()
        previousDay = 0
        totalDelta = 0
        firstRun = True
        report = {}

        # Grab results and loop over results
        for row in c.execute('SELECT * FROM timelog WHERE t1 LIKE ?', (date,)):
            # Set current day
            day = self.date(row[1])

            # First run is special
            if firstRun is True:
                previousDay = day
                firstRun = False

            # Calculate delta
            # If t2 is not set in database (because we're clocked in) use
            # the current time as t2 for delta calculation
            delta = self.delta(
                row[1],
                t2=str(now) if row[2] is None else row[2])

            if day == previousDay:
                # Sum up delta as long we're on the same day
                totalDelta += delta
            else:
                # Reset delta timer if we're not on the same day
                totalDelta = delta

            # Prepare for next run
            previousDay = day

            # Update report with new delta time
            report[day] = [day, totalDelta, (self.dailyWorkMinutes-totalDelta)]
        
        return report

    def printReport(self, date):
        """
        Print report using data gathered from reportObject

        :return: None
        """
        report = {"intro": ["Date", "Work", "Left"]}
        report.update(self.reportObject(date))

        for _, v in report.items():
            print("{} | {} | {}".format(
                self.cl(10, v[0]),
                self.cr(5, v[1]),
                self.cr(5, v[2])))

    def amz(self, i):
        """
        Add Minor Zero

        :param i: integer
        :return: zero formatted string if i is less then 10
        """
        return str(i) if i > 9 else "0{}".format(i)

    def date(self, stringTime):
        """
        Extract date from date string

        :param stringTime: date string in default python format
        :return: yyyy-mm-dd formatted date string
        """
        d = datetime.strptime(stringTime, "%Y-%m-%d %H:%M:%S.%f")
        return "{}-{}-{}".format(
            d.year,
            self.amz(d.month),
            self.amz(d.day))

    def time(self, stringTime):
        """
        Extract time from date string

        :param stringTime: date string in default python format
        :return: hh:mm formatted time string
        """
        d = datetime.strptime(stringTime, "%Y-%m-%d %H:%M:%S.%f")
        return "{}:{}".format(
            self.amz(d.hour),
            self.amz(d.minute))

    def delta(self, t1, t2):
        """
        Calculate delta from two time objects

        :param t1: datetime object 1
        :param t2: datetime object 2
        :return: Returns delta in minutes as integer
        """
        d1 = datetime.strptime(t1, "%Y-%m-%d %H:%M:%S.%f")
        d2 = datetime.strptime(t2, "%Y-%m-%d %H:%M:%S.%f")
        delta = (d2 - d1)
        return round(delta.total_seconds()/60)

    def cr(self, length, text):
        """
        Format column with text aligned right

        :param length: length of column
        :param text: Column text
        :return: string with whitespace added
        """
        return (length-len(str(text)))*" "+str(text)

    def cl(self, length, text):
        """
        Format column with text aligned left

        :param length: length of column
        :param text: Column text
        :return: string with whitespace added
        """
        return str(text)+(length-len(str(text)))*" "

    def getTodayDelta(self, date):
        """
        Print delta + amount owed in current month

        :return: None
        """
        report = self.reportObject(date)
        delta = 0
        
        for _, v in report.items():
            try:
                delta += v[2]
            except TypeError:
                pass

        if delta > 0:
            # Print delta if we still have worktime left
            print(delta)
            return True
        elif delta > -30:
            # Print leave message and overtime in parenthesis if we have no
            # more time left for today
            print("GTFO ({})".format(delta))
            return True
        else:
            # Print leave message and overtime in parenthesis if we have no
            # more time left for today
            print("GTFO ({})".format(delta))
            return False


if __name__ == "__main__":
    # Initialize argument parser
    parser = argparse.ArgumentParser(description='Clock timer')

    # This argument determines an action taken, in this case which type of
    # clock action that is taken. e.g. whether to clock in or out
    parser.add_argument('--type', default="get", type=str,
                        help='Type of clock (in/out)')

    # If type is 'out' then the comment argument can be specified, to add
    # a comment to the clockout. This could be used if the time tracking
    # database is used for a project where the allotted periods should have
    # a comment associated so it's easier to explain why the time was used
    # at a later time.
    parser.add_argument('--comment', default="", type=str,
                        help='Clock out comment')

    # This variable specified the path to the default database
    # The default database is used if this is not specified
    parser.add_argument('--file', default="foursevenfour.474",
                            type=str, help='Database file')

    # The date argument can be used to search periods on a specific day or
    # month, using the SQL query language - e.g. 2019-08-% returns everything
    # for august 2019. 2019-% returns everything for 2019. And 2019-08-10%
    # returns everything for the 10th of august 2019.
    now = datetime.now()
    parser.add_argument('--date',
                        type=str,
                        help='Date to search for in report',
                        default="{}-{}-%".format(
                            now.year,
                            now.month if now.month > 9 else "0{}".format(now.month)))

    # This argument returns the timedelta for today - this can be used when
    # the script is called from a status bar.
    parser.add_argument('--today', action='store_true',
                        help='Print todays work time')

    # The minutes argument specifies the amount of minutes worked in a day
    # It is used to calculate the delta when using the --today
    parser.add_argument('--minutes', default=444, type=int,
                        help='Minutes to work in a day')

    # Parse arguments
    args = parser.parse_args()

    # Start program
    tt = fourSevenFour(args.file)
    if tt.validateDatabase():
        tt.createDatabase()
        tt.setDailyMinutes(args.minutes)

        if args.today is True:
            if tt.getTodayDelta(args.date):
                exit(0)
            else:
                exit(1)
        elif args.type == "get":
            tt.printReport(args.date)
        elif args.type == "in":
            tt.clockIn()
        elif args.type == "out":
            tt.clockOut(args.comment)

        tt.close()
