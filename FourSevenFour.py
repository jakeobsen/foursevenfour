#!/usr/bin/env python3

#
# IMPORTANT NOTICE:
# This script was my initial script, that spawned this project. this script
# was never git tracked, and i only decided to add it as a historical reference.
#
# This script will never see any updates, and you may use it at your own will.
#

from datetime import datetime
import argparse

def clock():
    clock = datetime.now()
    with open("/home/mj/.timetrack", 'w', encoding='utf-8') as clockFile:
        clockFile.write(str(clock))

def stop():
    with open("/home/mj/.timetrack", 'w', encoding='utf-8') as clockFile:
        clockFile.write("stop")

def diff():
    with open("/home/mj/.timetrack", 'r', encoding='utf-8') as inFile:
        t2 = inFile.readline().strip()

    if t2 == "stop":
        r = False
    else:
        t1 = str(datetime.now())
        d1 = datetime.strptime(t1, "%Y-%m-%d %H:%M:%S.%f")
        d2 = datetime.strptime(t2, "%Y-%m-%d %H:%M:%S.%f")
        r = (d1 - d2)

    return r

parser = argparse.ArgumentParser(description='Clock timer')
parser.add_argument('--type', default="get", type=str, help='Type of clock (in/out)')
args = parser.parse_args()

if args.type == "get":
    with open("/home/mj/.timetrack", 'r', encoding='utf-8') as f:
        status = f.readline()

    time = diff()
    if time is not False:
        delta = round(time.total_seconds()/60)
        result = (474 - delta)
        if result > 0:
            print(result)
        else:
            print("GTFO ({})".format(result))
elif args.type == "in":
    clock()
elif args.type == "stop":
    stop()

